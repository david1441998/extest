import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public errorMessage:string;
  constructor(private authService:AuthService,
    private router:Router) { this.authService.getLoginErrors().subscribe(error=>{
              this.errorMessage=error;
            });
           }
  email:string;
  password:string; 

  onSubmit(){
    this.authService.login(this.email,this.password);
  //  this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

}
