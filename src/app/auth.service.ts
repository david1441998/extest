import { Router } from '@angular/router';
import { User } from './user';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  private logInErrorSubject = new Subject<string>();
  private signUpErrorSubject = new Subject<string>();

  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }

  signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful sign up',res);
            this.router.navigate(['/succes']);
            
          }
                ).catch (error => this.signUpErrorSubject.next(error.message))
              console.log(this.signUpErrorSubject);
            
          }
        

  

  logout(){
    this.afAuth.auth.signOut();  
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/succes']);
                    }
                    ).catch(error => this.logInErrorSubject.next(error.message));
               
                }
                  public getLoginErrors(): Subject<string> {
                      return this.logInErrorSubject;
                    }
                    public getSignUpErrors(): Subject<string> {
                      return this.signUpErrorSubject;
                    }
  }




