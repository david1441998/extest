// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyCHkh-p07_OGGIX8WkW6Iq2zI_ml-RrjMA",
    authDomain: "test-c9cf8.firebaseapp.com",
    databaseURL: "https://test-c9cf8.firebaseio.com",
    projectId: "test-c9cf8",
    storageBucket: "test-c9cf8.appspot.com",
    messagingSenderId: "868900275675",
    appId: "1:868900275675:web:6277eba4da63e20f73d3fe",
    measurementId: "G-PCMGZQWWBV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
